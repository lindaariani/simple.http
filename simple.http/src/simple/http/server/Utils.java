package simple.http.server;

import java.util.Map;

import simple.http.types.HttpResponse;
import simple.http.types.StatusLine;

/**
 * @author Linda Ariani Gunawan
 * @version 1.0.0
 *
 */
public class Utils {

	public static HttpResponse genRespBadReq() {
		StatusLine sl = new StatusLine("HTTP/1.1", 400, "Bad Request");
		return new HttpResponse(sl, null, null);
	}

	public static HttpResponse genRespOK() {
		return Utils.genRespOK(null, null);
	}

	public static HttpResponse genRespOK(Map<String,String> header) {
		return Utils.genRespOK(header, null);
	}

	public static HttpResponse genRespOK(String body) {
		return Utils.genRespOK(null, body);
	}

	public static HttpResponse genRespOK(Map<String,String> header, String body) {
		StatusLine sl = new StatusLine("HTTP/1.1", 200, "OK");
		return new HttpResponse(sl, header, body);
	}
	
	public static HttpResponse genRespCreated(Map<String,String> header, String body) {
		StatusLine sl = new StatusLine("HTTP/1.1", 201, "Created");
		return new HttpResponse(sl, header, body);
	} 

}
