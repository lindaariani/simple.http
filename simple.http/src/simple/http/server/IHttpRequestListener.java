/**
 * 
 */
package simple.http.server;

import simple.http.types.HttpRequest;
import simple.http.types.HttpResponse;

/**
 * @author Linda Ariani Gunawan
 * @version 1.0.0
 *
 */
public interface IHttpRequestListener {
	
	//if req == null then bad request and the server will automatically send 400 Bad Request
	public HttpResponse receiveRequest(HttpRequest req);
}
