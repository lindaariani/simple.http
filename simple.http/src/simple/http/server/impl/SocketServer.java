package simple.http.server.impl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import simple.http.server.IHttpRequestListener;

/**
 * @author Linda Ariani Gunawan
 * @version 1.0.0
 *
 */
public class SocketServer implements Runnable {
	private IHttpRequestListener reqListener;
	private ServerSocket server;
	private boolean isActive;
	
	public SocketServer(int port, IHttpRequestListener reqListener) throws IOException {
		this.reqListener = reqListener;
		server = new ServerSocket(port);
		isActive = true;
	}
	
	
	public synchronized void stop() {
		isActive = false;
		try {
			if (server != null) {
				server.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while(isActive) {
			try {
				Socket socket = server.accept();
				Thread t = new Thread(new ClientWorker(socket, reqListener));
				t.start();
			} catch (IOException e) {
				if (!isActive) {
					//normal stop
				} else {
					e.printStackTrace();
				}
			}
		}
    }
}
