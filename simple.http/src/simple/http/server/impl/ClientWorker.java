package simple.http.server.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.StringTokenizer;

import simple.http.server.IHttpRequestListener;
import simple.http.server.Utils;
import simple.http.types.HttpMethod;
import simple.http.types.HttpRequest;
import simple.http.types.HttpResponse;

/**
 * @author Linda Ariani Gunawan
 * @version 1.0.0
 *
 */
public class ClientWorker implements Runnable {

	private IHttpRequestListener reqListener;
	private Socket socket;
	private PrintWriter out;

	public ClientWorker(Socket socket, IHttpRequestListener reqListener) {
		this.socket = socket;
		this.reqListener = reqListener;
	}

	@Override
	public void run() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader (socket.getInputStream()));                  
			out = new PrintWriter(socket.getOutputStream());

			//read the first line, i.e., the request line
			//format: method absolute-or-path-uri HTTP/1.1
			String line = in.readLine();

			if (line == null) {
				//bad request line
				badReqHandler(out);
				return;
			}

			StringTokenizer tokenizer = new StringTokenizer(line);
			HttpMethod method = toMethod(tokenizer.nextToken());
			if (method == null) {
				//bad request, wrong method
				badReqHandler(out);
				return;
			}
			HttpRequest request = new HttpRequest();
			request.setMethod(method);

			//either absolute (starts with http://) or path uri (starts with /)
			String uri = tokenizer.nextToken();
			if (uri == null) {
				//bad request
				badReqHandler(out);
				return;
			}

			String body = "";
			boolean isBody = false;
			while (in.ready()) {
				line = in.readLine();
				if (!isBody) {
					if (line.length() == 0) {
						//empty string, so what comes next is the body
						isBody = true;
						continue;
					} else {
						//header
						String key, value;
						int idx = line.indexOf(":");
						if (idx != -1) {
							key = line.substring(0, idx).trim();
							value = line.substring(idx + 1).trim();
							request.addHeaderItem(key, value);
						} else {
							//wrong format
							badReqHandler(out);
							return;
						}
					}
				} else {
					body = body + line;
				}
			}

			request.setBody(body);

			String host = request.getHeaderItem("Host");
			try {
				if (host != null) {
					//uri is not absolute
					request.setUrl(new URL("http://" + host + uri));
				} else {
					//uri must be absolut
					request.setUrl(new URL(uri));
				}
			} catch (MalformedURLException e) {
				//bad request, wrong method
				badReqHandler(out);
				return;
			}

			//notify listener
			HttpResponse resp = reqListener.receiveRequest(request);
			out.write(resp.generateResponse());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void badReqHandler(PrintWriter out) throws IOException {
		reqListener.receiveRequest(null);
		out.write(Utils.genRespBadReq().generateResponse());
		out.close();
	}

	private HttpMethod toMethod(String method) {
		if (method.equalsIgnoreCase("GET")) {
			return HttpMethod.GET;
		} else if (method.equalsIgnoreCase("POST")) {
			return HttpMethod.POST;
		} else if (method.equalsIgnoreCase("PUT")) {
			return HttpMethod.PUT;
		} else if (method.equalsIgnoreCase("DELETE")) {
			return HttpMethod.DELETE;
		}
		return null;
	}
}
