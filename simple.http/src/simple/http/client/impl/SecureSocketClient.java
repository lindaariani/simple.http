package simple.http.client.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import simple.http.client.IHttpClient;
import simple.http.types.HttpRequest;
import simple.http.types.HttpResponse;
import simple.http.types.StatusLine;

public class SecureSocketClient implements IHttpClient {

	public static final int CR = 13; // <US-ASCII CR, carriage return (13)>
	public static final int LF = 10; // <US-ASCII LF, linefeed (10)>

	private String host;
	private int port = 443;
	private Socket socket = null;
	private Logger logger = LoggerFactory.getLogger(SecureSocketClient.class);

	public SecureSocketClient(String host, int port) {
		super();
		this.host = host;
		this.port = port;
	}

	public SecureSocketClient(String host) {
		super();
		this.host = host;
	}

	public HttpResponse send(HttpRequest request) throws IOException {
		try {
			logger.trace("Request: method=" + request.getMethod() + ", url=" + 
					request.getUrl() + ", httpcmd=" + request.getHttpCommand() + "END");

			SocketFactory socketFactory = SSLSocketFactory.getDefault();
			socket = socketFactory.createSocket(host, port);
			logger.trace("Secure socket connected. host=" + host + ", port=" + port + "");

			PrintWriter out = new PrintWriter(socket.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			logger.trace("Output and input streams opened");

			out.println(request.getHttpCommand());
			out.flush();
			logger.trace("Request sent");

			HttpResponse hr = new HttpResponse();
			String str = in.readLine();
			logger.trace("Response status line=" + str);

			String status0 = "";
			String status1 = "0";
			String shortMessage = "";

			if (str != null) {
				String status[] = str.split(" ");
				if (status.length > 0) {
					status0 = status[0];
				}
				if (status.length > 1) {
					status1 = status[1];				
				}

				if (status.length > 2) {			
					shortMessage = status[2];
				}
				for (int i = 3;i < status.length;i++) {
					shortMessage += " " + status[i];
				}						
			}

			StatusLine sl = new StatusLine(status0, Integer.parseInt(status1), shortMessage);
			hr.setStatus(sl);

			//header
			int length = -1;
			while ((str = in.readLine()) != null) {
				if (str.length() == 0) {
					//start of body
					break;
				}

				int idx = str.indexOf(":");
				String name = str.substring(0, idx);
				String value = str.substring(idx + 1).trim();
				hr.addHeaderItem(name, value);
				logger.trace("Header::  " + str);
				if (name.equals("Content-Length")) {
					length = Integer.parseInt(value);
				}
			}

			//body
			String body = "";
			if (length == 0) {
				//header contains Content-Length, but zero
				//do not need to read the body part
			} else if (length > 0) {
				//header contains Content-Length
				char[] cbuf = new char[length];
				in.read(cbuf, 0, length);
				body = new String(cbuf);
//				logger.debug("body=" + body + "END");
			} else {
				//header does not contain Content-Length, so read the body anyway
				//assume CR-LF-CR-LF marks the end of body
				in.readLine();
				char[] buff = new char[1024];

				int ret = 0;
				ret = in.read(buff, 0, 1024);
				while (ret != -1) {
					if (ret >= 4 && buff[ret-1] == LF && buff[ret-2] == CR 
							&& buff[ret-3] == LF && buff[ret-4] == CR) {
						for (int i = 0; i < ret-5; i++) {
							body = body + buff[i];
						}
						break;
					} else {
						for (int i = 0; i < ret; i++) {
							if (buff[i] == CR) {
								body = body + "\n";
							} if (buff[i] == LF) {
							} else {
								body = body + buff[i];
							}
						}
					}
					ret = in.read(buff, 0, 1024);
				}
//				logger.trace("body=" + body + "END");
			}
			body = body.trim();
			hr.setBody(body);

			return hr;
		} catch (UnknownHostException e) {
			logger.trace("UnknownHostException", e);
			throw e;
		} catch (IOException e) {
			logger.trace("IOException", e);
			throw e;
		} finally {
			if (socket != null) {
				try {
					socket.close();

					logger.trace("Socket closed");
				} catch (IOException e) {
					logger.trace("IOException", e);
				}
			}
		}
	}

	public void close() {
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
				logger.info("Closing socket exception. {}", e.getLocalizedMessage());
			}
		}
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}
}
