package simple.http.client;

import simple.http.types.HttpRequest;
import simple.http.types.HttpResponse;

/**
 * @author Sam Wilson
 * @version 1.1
 *
 * updated by Linda Ariani Gunawan - 2012-02-17 16:44
 */

public interface IHttpClient {

	public HttpResponse send(HttpRequest request) throws Exception;
}
