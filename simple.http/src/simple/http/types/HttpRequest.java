package simple.http.types;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Models an HTTP request
 * 
 * @author Sam Wilson
 * @version 1.1
 * 
 * Modified by Linda Ariani Gunawan - 2012-02-17 14:50
 *
 */
public class HttpRequest {

	/**
	 * URL the HttpRequest is intended for
	 */
	private URL url;

	/**
	 * HTTP Method that the object uses, default: GET method
	 */
	private HttpMethod method = HttpMethod.GET;

	/**
	 * Hashtable of the Header identifier and its value
	 */
	private Map<String, String> header = new HashMap<String, String>();

	/**
	 * Body of the HTTP Request
	 */
	private String body = "";

	/**
	 * Constructor
	 * @param url
	 * @param method
	 * @param header
	 * @param body
	 */
	public HttpRequest(URL url, HttpMethod method,
			Map<String, String> header, String body) {
		super();
		this.method = method;
		this.header = header;
		this.body = body;
		this.url = url;
	}

	/**
	 * Default Constructor
	 */
	public HttpRequest() {
	}

	/**
	 * Constructor
	 * @param a The URL that this Request is intended for
	 */
	public HttpRequest(String a) {
		super();
		try {
			this.url = new URL(a);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Constructor
	 * @param url
	 * @param method
	 * @param body
	 */
	public HttpRequest(URL url, HttpMethod method, String body) {
		this.method = method;
		this.body = body;
		this.url = url;
	}

	/**
	 * Add an header item, this is stored as a Key,Value pair.
	 * Can be used to set session values, and send cookie information
	 * @param Key
	 * @param Value
	 */
	public void addHeaderItem(String Key, String Value) {
		this.header.put(Key, Value);
	}

	/**
	 * Generate the HTTP Command from the object
	 * @return Http Command
	 */
	public String getHttpCommand() {
		if (url == null) {
			return null;
		}
		if (method == null) {
			method = HttpMethod.GET;
		}
		if (header == null) {
			header = new HashMap<String,String>();
		}
		if (body == null) {
			body = "";
		}

		String ret = "";
		if (this.method.equals(HttpMethod.GET)) {
			if (url.getPath() == null || url.getPath().equals("")) {
				ret = "GET /";
			} else {
				ret = "GET " + this.url.getPath();
				if (url.getQuery() != null) {
					ret = ret + "?" + url.getQuery();
				}
			}
			ret = ret + " HTTP/1.1\r\n" + "Host: " + this.url.getHost();
			if (url.getPort() != -1 && url.getPort() != 80) {
				ret = ret + ":" + url.getPort();
			}
			ret = ret + "\r\n";

			ret = ret + this.getAllHeader();

			ret = ret + "\r\n";
		} else if (this.method.equals(HttpMethod.POST)) {

			ret = "POST " + this.url.getPath() + " HTTP/1.1\r\n" + "Host: "
					+ this.url.getHost();
			if (url.getPort() != -1 && url.getPort() != 80) {
				ret = ret + ":" + url.getPort();
			}
			ret = ret + "\r\n";

			ret = ret + this.getAllHeader();

			ret = ret + "Content-Length: " + this.body.length() + "\r\n\r\n"
					+ this.body + "\r\n";

		} else if (this.method.equals(HttpMethod.PUT)) {

			ret = "PUT " + this.url.getPath() + " HTTP/1.1\r\n"
					+ "Host: " + this.url.getHost();
			if (url.getPort() != -1 && url.getPort() != 80) {
				ret = ret + ":" + url.getPort();
			}
			ret = ret + "\r\n";

			ret = ret + this.getAllHeader();

			ret = ret + "Content-Length: " + this.body.length() + "\r\n\r\n"
					+ this.body + "\r\n";

		} else if (this.method.equals(HttpMethod.DELETE)) {

			ret = "DELETE " + this.url.getPath() + " HTTP/1.1\r\n"
					+ "Host: " + this.url.getHost();
			if (url.getPort() != -1 && url.getPort() != 80) {
				ret = ret + ":" + url.getPort();
			}
			ret = ret + "\r\n";

			ret = ret + this.getAllHeader();
			
			ret = ret + "\r\n";
		}

		return ret;
	}

	private static final String USER_AGENT_KEY = "User-Agent";
	private static final String USER_AGENT_VALUE = "SimpleHTTP";
	
	public String getAllHeader() {
		StringBuffer sb = new StringBuffer();

		Set<Entry<String,String>> entries = header.entrySet();
		boolean foundUserAgent = false;
		for (Entry<String,String> e : entries) {
			if (e.getKey().equals(USER_AGENT_KEY)) {
				foundUserAgent = true;
			}
			sb.append(e.getKey() + ": " + e.getValue() + "\r\n");
		}
		if (!foundUserAgent) {
			sb.append(USER_AGENT_KEY + ": " + USER_AGENT_VALUE + "\r\n");
		}

		return sb.toString();
	}

	/**
	 * @return the url
	 */
	public URL getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(URL url) {
		this.url = url;
	}

	/**
	 * @return the method
	 */
	public HttpMethod getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(HttpMethod method) {
		this.method = method;
	}

	/**
	 * @return the header
	 */
	public Map<String, String> getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(Hashtable<String, String> header) {
		this.header = header;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	public String getHeaderItem(String Key) {
		if (header == null) {
			return null;
		}
		return header.get(Key);
	}

}
