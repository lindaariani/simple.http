package simple.http.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * Models an HTTP response
 * 
 * @author Sam Wilson
 * @version 1.1
 * 
 * Modified by Linda Ariani Gunawan - 2012-02-17 14:50
 *
 */
public class HttpResponse {
	/**
	 * Status of the Response
	 */
	private StatusLine statusLine;

	/**
	 * Body of the Response
	 */
	private String body;

	/**
	 * Header information, there is a Item called "Status" which is the HTTP
	 * status code of the Response
	 */
	private Map<String, String> header = new HashMap<String, String>();

	/**
	 * Constructor
	 */
	public HttpResponse() {
	}
	
	public HttpResponse(StatusLine statusLine, Map<String,String> header, String body) {
		this.statusLine = statusLine;
		this.header = header;
		this.body = body;
	}
	
	/**
	 * Add an item to the Response header, this method is intended only to be
	 * used by the HttpClient.
	 * 
	 * @param Key
	 * @param Value
	 */
	public void addHeaderItem(String Key, String Value) {
		if (header == null) {
			header = new HashMap<String, String>();
		}
		header.put(Key, Value);
	}

	/**
	 * Get an item from the Response header, there is an Item called "Status"
	 * which is the HTTP stutus code of the Response
	 * 
	 * @param Key
	 * @return the item
	 */
	public String getHeaderItem(String Key) {
		if (header == null) {
			return null;
		}
		return header.get(Key);
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the status
	 */
	public StatusLine getStatusLine() {
		return statusLine;
	}

	/**
	 * @param statusLine the statusLine to set
	 */
	public void setStatus(StatusLine statusLine) {
		this.statusLine = statusLine;
	}

	/**
	 * @return the header
	 */
	public Map<String, String> getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(Map<String, String> header) {
		this.header = header;
	}

	public String generateResponse() {
		StringBuffer sb = new StringBuffer();
		sb.append(statusLine.toString() + "\r\n");
		if (header != null) {
			sb.append(getAllHeader());
		}
		if (body != null && body.length() > 0) {
			sb.append("Content-Length: " + body.length() + "\r\n\r\n");
			sb.append(body);
		}
		return sb.toString();
	}
	
	
	private String getAllHeader() {
		StringBuffer sb = new StringBuffer();

		Set<Entry<String,String>> entries = header.entrySet();
		for (Entry<String,String> e : entries) {
			if (e.getKey().equalsIgnoreCase("Content-Length")) {
				continue;
			}
			sb.append(e.getKey() + ": " + e.getValue() + "\r\n");
		}

		return sb.toString();
	}
}
