/**
 * 
 */
package simple.http.types;

/**
 * @author Linda Ariani Gunawan
 * @version 0.0.1
 *
 */
public class StatusLine {
	
	private String protocolVersion;
	private int statusCode;
	private String shortPhrase;

	/**
	 * Constructor 
	 */
	public StatusLine() {
	}

	/**
	 * @param protocolVersion
	 * @param statusCode
	 * @param shortPhrase
	 */
	public StatusLine(String protocolVersion, int statusCode,
			String shortPhrase) {
		this.protocolVersion = protocolVersion;
		this.statusCode = statusCode;
		this.shortPhrase = shortPhrase;
	}

	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * @param protocolVersion the protocolVersion to set
	 */
	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the shortPhrase
	 */
	public String getShortPhrase() {
		return shortPhrase;
	}

	/**
	 * @param shortPhrase the shortPhrase to set
	 */
	public void setShortPhrase(String shortPhrase) {
		this.shortPhrase = shortPhrase;
	}
	
	@Override
	public String toString() {
		return protocolVersion + " " + statusCode + " " + shortPhrase;
	}
}
