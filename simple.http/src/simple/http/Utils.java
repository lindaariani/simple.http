package simple.http;

public class Utils {
	public static String toHex(String s) {
		StringBuilder sb = new StringBuilder();
		for (byte b : s.getBytes()) {
			sb.append(String.format("%02x", b&0xff));
			sb.append(" ");
		}
		return sb.toString();
	}
}
